package qiwu.framwork;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan(basePackages = {"qiwu.framwork.aop", "qiwu.framwork.business"})
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AppConfig {


}