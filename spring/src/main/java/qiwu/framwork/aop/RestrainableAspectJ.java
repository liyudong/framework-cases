package qiwu.framwork.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RestrainableAspectJ {
    @Around("@annotation(restrainable)")
    public Object process(ProceedingJoinPoint point, Restrainable restrainable) throws Throwable {

        //获取方法参数值数组
        Object[] args = point.getArgs();
        Object result = point.proceed(args);

        System.out.println("RestrainableAspectJ.process");

        return result;
    }
}