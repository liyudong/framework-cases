package qiwu.framwork;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import qiwu.framwork.business.NotifyService;

public class AppTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        NotifyService notifyService = context.getBean("notifyService", NotifyService.class);

        notifyService.notifyInfluence("", "");
    }
}